/*
 * proyectofin.cpp
 * 
 * Copyright 2019 Vladimir <vladimir@ciencias.unam.mx>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdlib.h>
#include<math.h>
#include <iostream>
#include "random.h"

  //Un sistema de ising
using namespace std;  

FILE* datos=fopen("proyectofin10.dat","w"); 
FILE* gnuplot=popen("gnuplot","w"); 

    class Ising {  //Definimos la clase Espin
		public:  
			Ising();
			void setsize();
			void llenadonegativo();
			void llenadopositivo();
			int asig();
			void val();
			void ener(); //Definimos todos los metodos 
			void magne();
			void printespin(double n);
			void intercambio();
			void reset();
			void diferenciaener();
			void uniarreglo();
			void settemperatura(double temp);
			int aceptacion();
			void renovar();
			void equilibracion();
			void setoptions();
			void promedios();
			double absoluto(double a);
			void printplot();
                 
		private:
			int ising[100][100];
			int size; 
			int nespin;
			int sweeps;
			double energia;
			double magnetizacion;
			double temperatura;
			double difenergia;
			int x;
			int y;
			int k;
			double promagne;
			double promener;
			double promagneabs;
			double promagnecuad;
			double promenercuad;
			double promagnecuart;
			double varianzac;
			double varianzapsi;
			double kumulante;
	};
    

	
	
	Ising::Ising() {  //Inicializador
		for(int i=0;i<100;i++) {
			for(int j=0;j<100;j++) {
				ising[i][j]=0;
			}
		}
		size=101;
		nespin=101*101;
		energia=0.0;
		magnetizacion=0.0;
		temperatura=0.0;
		difenergia=0.0;
		x=0;
		y=0;
		k=1;
		sweeps=20000;
		promagne=promagneabs=promagnecuad=promener=promenercuad=0.0;
		varianzac=varianzapsi=kumulante=0.0;
	}
	
	void Ising::llenadopositivo() {
		for(int i=1;i<size+1;i++) {
                       for(int j=1;j<size+1;j++) {
                               ising[i][j]=1;
                       }
        	}
	}

	void Ising::llenadonegativo() {
		for(int i=1;i<size+1;i++) {
			for(int j=1;j<size+1;j++) {
				ising[i][j]=-1;
			}
		}
	}        

	int Ising::asig() {   //Asignamos 1 o -1 aleatoriamente
		double a=drand();   
		if (a>=0&&a<0.5)
			a=-1;   
		else
			a=1;//La funcion que da probabilidades
		return int (a); 
	}

	void Ising::setsize() {//Especificamos el tamaño del arreglo
		cout<<"Solo arreglos cuadrados.\n";
		cout<<"¿De que tamaño va a ser el lado tu arreglo (LXL)?\n";
		cin>>size;
		if (size>101)
			cout<<"Debe ser menor de 100\n";
		nespin=size*size;
//     cout<<"Cuantos sweeps?:\n";
//     cin>>sweeps;
	}

	void Ising::settemperatura(double t) {//Damos valor a la temperatura
		temperatura=t;
	}

	int Ising::aceptacion() {//Decide si acepta cambio o no
		double ale=drand();
		int bole;
		if (ale<=exp(-difenergia/(k*temperatura))) {
			bole=1;
			
			
		}
		
		else {
       //cout<<"No aceptado\n";
			bole=0;
		}
			
		return bole;
			
	}

	void Ising::val() {//asignamos valores al arreglo con asig()
		for(int i=1;i<size+1;i++) {
			for(int j=1;j<size+1;j++) {
				ising[i][j]=asig();
			}
		}
	}
                    
            

	void Ising::ener() {//Calculamos la energia
    		int prenergia=0;
    		int a, b, c, d;
    		for(int i=1;i<size+1;i++) {
                	for(int j=1;j<size+1;j++) {
                        	/*a=i-1;
				if (a<1)
					ising[a][j]=0;
				b=j-1;
				if (b<1)
					ising[b][j]=0;*/               
				prenergia+=ising[i][j]*(ising[i-1][j]+ising[i][j-1]+ising[i+1][j]+ising[i][j+1]);
       }
	}
    	energia=-prenergia*0.5;
}

	void Ising::magne() {//Calculamos la magnetizacion
		
		magnetizacion = 0;
		
		for(int i=1;i<size+1;i++) {
			for(int j=1;j<size+1;j++) {
				magnetizacion+=ising[i][j];
			}
		}
		magnetizacion=magnetizacion;
	}

	double Ising::absoluto(double n) {
		if (n>=0)
			return n;
		else
			return -n;
	}

	void Ising::intercambio() {//Intercambia un espin al azar
    //int nuevo;
    //cout<<px<<"\t"<<py<<"\n";
		x=int(drand()*size)+1;  
		y=int(drand()*size)+1;
    //cout<<nx<<"\t"<<ny<<"\n";
    //nuevo=(-1)*ising[x][y];
    //ising[x][y]=nuevo;
	}

	void Ising::reset() {//Regresa los valores a cero
		//energia=0.0;
		magnetizacion=0.0;
		//difenergia=0.0;
	}
     
	void Ising::diferenciaener() {//Calculamos la diferencia
		/*int izquierdo=0,derecho=0,arriba=0,abajo=0;
		izquierdo=x-1;
		if (izquierdo<1)
        		ising[izquierdo][y]=0;
		arriba=y-1;
		if (arriba<1)
        		ising[x][arriba];*/
     		difenergia=(ising[x][y]+ising[x][y])*(ising[x-1][y]+ising[x+1][y]+ising[x][y-1]+ising[x][y+1]);             
	}

	void Ising::renovar() {
		if (difenergia<=0.0){
			ising[x][y]=-ising[x][y];
			energia+=difenergia;
			}
		
		else{
			aceptacion();
			if (aceptacion()==1){
				ising[x][y]=-ising[x][y];
				energia+=difenergia;
				}
		}
		
	}
     
     
     

	void Ising::printespin(double n) {//Imprimimos valores
		cout<<n<<" <M>=  "<<promagne<<" <E>= "<<promener<<" <M2>= "<<promagnecuad<<" <E2> "<<promenercuad<<" <|M|>= "<<promagneabs<<" c= "<<varianzac<<" psi= "<<varianzapsi<<" k= "<<kumulante<<endl;
		fprintf(datos,"%lf\t  %lf \t  %lf \t %lf \t %lf \t %lf \t %lf \t %lf \t %lf\n",n,promagne,promener,promagnecuad,promenercuad,promagneabs,varianzac, varianzapsi,kumulante);
	}

	void Ising::printplot() {
		FILE* archivo=fopen("imagen.dat","w");
        	for(int i=0;i<=size+1;i++) {
			for(int j=0;j<=size+1;j++) {
				fprintf(archivo," %d ",ising[i][j]);
			}
			fprintf(archivo,"\n");
		}
		fclose(archivo);
		fprintf(gnuplot,"plot 'imagen.dat' matrix w image \n");
		fflush(gnuplot);
	}

	void Ising::setoptions() {
		int selec;
		cout<<"Si quieres llenar con todos 1 aprieta 1, con todos -1 escribe -1, aleatorio 0\n";
		cin>>selec; 
		setsize();
		if (selec==0) {
			asig();
			val();
		}
		else if (selec==-1) {
			llenadonegativo();
		}
		else if (selec==1) {
			llenadopositivo();
		} 
	}   
    
    
   
	void Ising::equilibracion() {
    //int n;
		int selec;
		sweeps=1000;    
                ener();
		for(int n=0;n<sweeps;n++) {  
			for(int j=1; j<=size*size;j++) {  
				intercambio();
				//reset();  
				
				
            //s.ener();
            //s.magne();
				diferenciaener();
            //s.aceptacion();
				renovar();
			}    
//     s.ener();
//     s.magne();
    //printespin(n+1);
		}
	}

	void Ising::promedios() {
		double sumamag=0.0,sumaener=0.0,sumamagcuad=0.0,sumaenercuad=0.0,sumamagabs=0.0,sumamagcuart=0.0;
		int selec;
		sweeps=10000; 
		kumulante=0.0;
    //equilibracion();   
    //ener();
    //magne();
    //diferenciaener();
    //printespin(0); 
		for(int n=0;n<sweeps;n++) {  
			for(int j=1; j<=size*size;j++) {  
				intercambio();
				//reset();
				
				
            //s.ener();
            //s.magne();
				diferenciaener();
            //s.aceptacion();
				renovar();
			}    
			
		
			//ener();
			magne();
						
			
     	//printespin(n+1);
			sumamag+=magnetizacion/nespin;
			sumaener+=energia/nespin;
			sumamagcuad+=magnetizacion*magnetizacion/(nespin*nespin);
			sumaenercuad+=energia*energia/(nespin*nespin);
			
			
			
			sumamagabs+=absoluto(magnetizacion)/nespin;
			sumamagcuart+=(magnetizacion/nespin)*(magnetizacion/nespin)*(magnetizacion/nespin)*(magnetizacion/nespin);
//	printespin(n+1);
		}
		promagne=sumamag/sweeps;
		
		promener=sumaener/sweeps;
		promagnecuad=sumamagcuad/sweeps;
		promenercuad=sumaenercuad/sweeps;
		promagneabs=sumamagabs/sweeps;
		promagnecuart=sumamagcuart/sweeps;
		varianzac=promenercuad-promener*promener;
		varianzapsi=promagnecuad-promagneabs*promagneabs;
		kumulante=1-(promagnecuart/(3*promagnecuad*promagnecuad));
	}
    
	int main() {
		Ising s;
		double t_inicial,t_final,delta_t;
		double pasos;
		cout<<"Dame la temperatura inicial (kelvins):"<<endl;
		cin>>t_inicial;
		cout<<"Dame la temperatura final (kelvins):"<<endl;
		cin>>t_final;
		cout<<"Cuantos pasos?:"<<endl;
		cin>>pasos;
		delta_t=(t_final-t_inicial)/pasos;
		s.setoptions();
		s.settemperatura(t_inicial);
		s.equilibracion();
		for(double t=t_inicial;t<=t_final;t+=delta_t) {
			s.settemperatura(t);
			s.promedios();
			s.printespin(t);
			s.printplot();
		}
		fclose(datos);
//s.printplot();
	}
