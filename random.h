/*
 * random.h
 * 
 * Copyright 2019 Vladimir <vladimir@ciencias.unam.mx>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

//Cabecera que guardara nuestras funciones 
//para generar numeros aleatorios


#include<stdlib.h>

double drand() {  //Genera numeros aleatorios entre 0 y 1
    return rand()/(RAND_MAX+1.0);
    }
    
double frand(double a, double b) { //Genera numeros aleatorios entre a y b
    return a+(b-a)*drand();
    }
    

